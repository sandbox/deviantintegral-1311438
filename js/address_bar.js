(function ($) {

/**
 * This behavior hides the address bar in iOS.
 */
Drupal.behaviors.hideAddressBar = {
  attach: function (context, settings) {
    $('body:not(.hide-address-bar-processed)').addClass('hide-address-bar-processed').each(function() {
      $(window).load(function() {
        setTimeout(function() {
          window.scrollTo(0, 1);
        });
      });
    });
  }
};

}(jQuery));

